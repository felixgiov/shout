package deepsense.scream;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.webkit.WebView;
import android.widget.TextView;

import com.firebase.client.DataSnapshot;
import com.firebase.client.Firebase;
import com.firebase.client.FirebaseError;
import com.firebase.client.ValueEventListener;

/**
 * Created by USERR on 26 Apr 2016.
 */
public class DescriptionActivity extends AppCompatActivity {

    int i=0;
    int posisiItemRecycler;
    TextView desc;
    TextView time;
    TextView date;
    TextView postedby;
    TextView nope;
  //  TextView linklink;
    WebView browser;
    Toolbar toolbar;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.description_activity);

        toolbar = (Toolbar) findViewById(R.id.toolbara);
        setSupportActionBar(toolbar);

        browser = (WebView) findViewById(R.id.webView);
        browser.getSettings().setLoadWithOverviewMode(true);
        browser.getSettings().setUseWideViewPort(true);

        desc = (TextView) findViewById(R.id.descView);
        time = (TextView) findViewById(R.id.timeView);
        date = (TextView) findViewById(R.id.dateView);
        nope = (TextView) findViewById(R.id.nomorhape);
    //    linklink = (TextView) findViewById(R.id.linkurl);
        postedby = (TextView) findViewById(R.id.namadiTV);

        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            posisiItemRecycler = extras.getInt("posisiItemRecycler");
        }

    }

    @Override
    protected void onStart() {
        super.onStart();

        Firebase ref = new Firebase("https://scream-and-suffer.firebaseio.com/messages");

        ref.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot snapshot) {

                System.out.println("There are " + snapshot.getChildrenCount() + " shout messages");
                for (DataSnapshot postSnapshot : snapshot.getChildren()) {
                    MessagePosts post = postSnapshot.getValue(MessagePosts.class);
//                    System.out.println(i + " " + post.getTitle() + " - " + post.getUsername());
//                    post.getDate();
//                    post.getTime();
//                    post.getDesc();
//                    post.getTitle();
//                    post.getUsername();
//                    post.getPhone();

                    if(i==posisiItemRecycler){

//                        System.out.println(i + " " + post.getTitle() + " - " + post.getUsername());
                        desc.setText(post.getDesc());
                        time.setText(post.getTime());
                        date.setText(post.getDate());
                    //    linklink.setText(post.getLinkurl());
                        postedby.setText("Posted by "+post.getUsername());
                        nope.setText("Contact details : "+post.getPhone());
                        getSupportActionBar().setTitle(post.getTitle());
                        browser.loadUrl(post.getLinkurl());

                    }

                    i++;
                }
            }
            @Override
            public void onCancelled(FirebaseError firebaseError) {
                System.out.println("The read failed: " + firebaseError.getMessage());
            }
        });
    }
}
