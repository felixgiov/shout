package deepsense.scream;

/**
 * Created by USERR on 23 Apr 2016.
 */

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.firebase.client.Firebase;
import com.firebase.client.Query;
import com.orm.SugarRecord;

import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class MessageAdder extends AppCompatActivity {

    EditText title;
    EditText phone;
    EditText desc;
    EditText linked;
    Toolbar toolbar;
    Firebase mReff;
    String shout;
    int cDay;
    int cMonth;
    int cYear;
    int cHour;
    int cMinute;
    String date;
    String time;
    String descr;
    String telphone;
    String linkalink;
    Query qRef;
    UserInfo info;
    List<UserInfo> listuser;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_message_adder);
        title = (EditText) findViewById(R.id.title_edit);
        phone = (EditText) findViewById(R.id.phone_edit);
        desc = (EditText) findViewById(R.id.desc_edit);
        linked = (EditText) findViewById(R.id.link_edit);
        toolbar = (Toolbar) findViewById(R.id.toolbar);

        listuser = SugarRecord.listAll(UserInfo.class);

        mReff = new Firebase("https://scream-and-suffer.firebaseio.com/messages");
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Enter Description");

        Button button = (Button) findViewById(R.id.shout_button);
        button.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Calendar calander = Calendar.getInstance();
                cDay = calander.get(Calendar.DAY_OF_MONTH);
                cMonth = calander.get(Calendar.MONTH) + 1;
                cYear = calander.get(Calendar.YEAR);
                cHour = calander.get(Calendar.HOUR_OF_DAY);
                cMinute = calander.get(Calendar.MINUTE);
                date = new String(cDay+"."+cMonth+"."+cYear);
                time = new String(cHour+":"+cMinute);

                linkalink = linked.getText().toString();

                if(linkalink.equals("")){
                    linkalink = new String("http://epaper2.mid-day.com/images/no_image_thumb.gif");
                }

                telphone = phone.getText().toString();
                descr = desc.getText().toString();
                shout = title.getText().toString();

                Map<String, String> post1 = new HashMap<String, String>();
                post1.put("username", listuser.get(0).getNamauser() );
                post1.put("title", shout);
                post1.put("desc", descr);
                post1.put("phone", telphone);
                post1.put("date", date );
            //    post1.put("tanggal", ServerValue.TIMESTAMP);
                post1.put("time", time);
                post1.put("linkurl", linkalink);
                mReff.push().setValue(post1);
                mReff.getKey();
                finish();

            }
        });
    }

}