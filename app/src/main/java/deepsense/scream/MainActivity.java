package deepsense.scream;

import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.firebase.client.Firebase;
import com.firebase.ui.FirebaseRecyclerAdapter;
import com.orm.SugarRecord;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    Firebase mRef;
    ArrayList<String> mMessages = new ArrayList<>();
    FloatingActionButton fab;
    RecyclerView mRecyclerView;
    Toolbar mtoolbar;
    public static String username;
    LinearLayoutManager mLayoutManager;
    int recyclerItemPosition;
  //  SharedPreference sharedPreference;
    UserInfo userInfo;
    List<UserInfo> listuser;
    ImageButton editprofile;
    String namague;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        editprofile = (ImageButton) findViewById(R.id.showMyPosts);

        recyclerItemPosition = -1;

    //    username = new String("Steve Rogers");

        listuser = SugarRecord.listAll(UserInfo.class);
        for(int i=0; i<listuser.size();i++){
            Log.d("vivasu",listuser.get(0).getNamauser());
        }

//        listuser.get(0).setNamauser("Shouter!");
        if(listuser.size()==0) {
            UserInfo userinfo = new UserInfo("Shouter!");
            userinfo.save();
            Log.d("vivakosong","Listnya kosong gan :)");
        }

        //  UserInfo userinfofound = UserInfo.findById(UserInfo.class,1);
        mtoolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(mtoolbar);

        editprofile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this,EditNameDialog.class);
                startActivity(intent);
            }
        });

        mtoolbar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                Intent intent = new Intent(MainActivity.this,EditNameDialog.class);
//                startActivity(intent);
            }
        });

        fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent i = new Intent(MainActivity.this,MessageAdder.class);
                startActivity(i);
            }
        });
        mRef = new Firebase("https://scream-and-suffer.firebaseio.com/messages");
        mRecyclerView = (RecyclerView) findViewById(R.id.recyclerView);
        mRecyclerView.setHasFixedSize(true);
        mLayoutManager = new LinearLayoutManager(this);
        mRecyclerView.setLayoutManager(mLayoutManager);
        mLayoutManager.setReverseLayout(true);
        mLayoutManager.setStackFromEnd(true);
    }

    public boolean isNetworkAvailable(final Context context) {
        final ConnectivityManager connectivityManager = ((ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE));
        return connectivityManager.getActiveNetworkInfo() != null && connectivityManager.getActiveNetworkInfo().isConnected();
    }

    @Override
    protected void onStart() {
        super.onStart();

        listuser = SugarRecord.listAll(UserInfo.class);

        namague = listuser.get(0).getNamauser();
        getSupportActionBar().setTitle("Hi, "+namague);
        Log.d("vivalavida",listuser.get(0).getNamauser());

        if (isNetworkAvailable(this)) {
          //  Toast.makeText(MainActivity.this, "Connected to Shout! Network", Toast.LENGTH_SHORT).show();
        } else {
            Toast toast = Toast.makeText(MainActivity.this,"Your request has not been processed due to connection error. Please check your connection and try again.", Toast.LENGTH_LONG);
            toast.setGravity(Gravity.CENTER, 0, 0);
            toast.show();
        }

        //Firebase messagesRef = mRef.child("messages");
        mRecyclerView.addItemDecoration(new DividerItemDecoration(this, mLayoutManager.VERTICAL));
        FirebaseRecyclerAdapter<MessagePosts , MessageViewHolder> adapter =
                new FirebaseRecyclerAdapter<MessagePosts, MessageViewHolder>(
                        MessagePosts.class,
                        R.layout.list_row,
                        MessageViewHolder.class,
                        mRef
                ) {
                    @Override
                    protected void populateViewHolder(MessageViewHolder messageViewHolder, MessagePosts s, int i) {
                        messageViewHolder.mTitle.setText(s.getTitle());
                        messageViewHolder.mUsername.setText(s.getUsername());
                        messageViewHolder.mDate.setText(s.getDate());
                        messageViewHolder.mTime.setText(s.getTime());
                    }
                };
        mRecyclerView.setAdapter(adapter);

        ItemClickSupport.addTo(mRecyclerView).setOnItemClickListener(new ItemClickSupport.OnItemClickListener() {
            @Override
            public void onItemClicked(RecyclerView recyclerView, int position, View v) {
//                Toast.makeText(MainActivity.this, "Posisi ke "+position, Toast.LENGTH_SHORT).show();

                recyclerItemPosition = position;
                Intent desc = new Intent(MainActivity.this,DescriptionActivity.class);
                desc.putExtra("posisiItemRecycler",recyclerItemPosition);
                startActivity(desc);
            }
        });

    }

    public static class MessageViewHolder extends RecyclerView.ViewHolder{
        TextView mTitle;
        TextView mUsername;
        TextView mDate;
        TextView mTime;

        public MessageViewHolder(View v){
            super(v);
            mTitle = (TextView) v.findViewById(R.id.title);
            mUsername = (TextView) v.findViewById(R.id.username);
            mDate = (TextView) v.findViewById(R.id.date);
            mTime = (TextView) v.findViewById(R.id.time);
        }
    }
}
