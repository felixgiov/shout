package deepsense.scream;

import android.content.Context;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.orm.SugarRecord;

import java.util.List;

/**
 * Created by USERR on 23 Apr 2016.
 */
public class EditNameDialog extends AppCompatActivity implements OnClickListener {

    Button save_btn , cancel_btn;
    EditText mEdit;
    String mEditText;
    UserInfo userInfo;
    List<UserInfo> listuser;
    InputMethodManager imm;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        //getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.fragment_edit_name);

        listuser = SugarRecord.listAll(UserInfo.class);

        save_btn = (Button) findViewById(R.id.button_save);
        cancel_btn = (Button) findViewById(R.id.button_discard);
        mEdit = (EditText) findViewById(R.id.edit_your_nameee);
        mEdit.setText(listuser.get(0).getNamauser());
        mEdit.requestFocus();
        imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.showSoftInput(mEdit, InputMethodManager.SHOW_IMPLICIT);
        save_btn.setOnClickListener(this);
        cancel_btn.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.button_save:
         //       MainActivity.username = mEditText;
//                UserInfo userInfo = UserInfo.findById(UserInfo.class,1);
//                userInfo.namauser = mEditText;
//                userInfo.save();
                mEditText = mEdit.getText().toString();
           //     listuser.get(0).setNamauser(mEditText);
                userInfo = listuser.get(0);
                userInfo.setNamauser(mEditText);
                userInfo.save();

                Log.d("vivalavidadadada",listuser.get(0).getNamauser());

                showToastMessage("Name Changed to "+ listuser.get(0).getNamauser());
                this.finish();

                break;

            case R.id.button_discard:

                showToastMessage("Discarded");
                this.finish();

                break;
        }

    }

    void showToastMessage(String message) {
        Toast.makeText(getApplicationContext(), message, Toast.LENGTH_SHORT)
                .show();
    }

}


