package deepsense.scream;

/**
 * Created by USERR on 24 Apr 2016.
 */
public class MessagePosts {
    private String username;
    private String title;
    private String desc;
    private String time;
    private String date;
    private String phone;
    private String linkurl;

    public MessagePosts() {
        // empty default constructor, necessary for Firebase to be able to deserialize blog posts
    }
    public String getUsername() {
        return username;
    }
    public String getTitle() {
        return title;
    }
    public String getDesc() {
        return desc;
    }
    public String getTime() {
        return time;
    }
    public String getDate() {
        return date;
    }
    public String getPhone() {
        return phone;
    }
    public String getLinkurl() {return linkurl;}
}
